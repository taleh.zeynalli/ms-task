package org.ingress.common.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreatedEvent {
    private UUID  id;
    private String displayName;
    private String email;
    private String password;
}
