package org.ingress.user.controller;

import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.queryhandling.QueryGateway;
import org.ingress.user.coreapi.commands.CreateUSerCommands;
import org.ingress.user.dto.UserDto;
import org.ingress.user.dto.UserResponse;
import org.ingress.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final CommandGateway commandGateway;
    private final QueryGateway queryGateway;

    @PostMapping("/user")
    public UUID handle(@RequestBody CreateUSerCommands command) {
        return commandGateway.sendAndWait(command);
    }

//    @GetMapping("{userId}")
//    public CompletableFuture<UserView> handle(@PathVariable("userId") String userId) {
//        return queryGateway.query(new FindUserQuery(UUID.fromString(userId)),
//                ResponseTypes.instanceOf(UserView.class));
//    }

    @PostMapping
    public ResponseEntity<UserResponse> createUser(@RequestBody UserDto userDto){
        UserResponse userResponse = userService.createUser(userDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(userResponse);
    }

//    @PutMapping("/{id}")
//    public UserResponse updateUser(@RequestBody UserDto userDto,@PathVariable("id") Long id){
//        UserResponse userResponse = userService.updateUser(userDto,id);
//        return userResponse;
//    }
public static void main(String[] args) {
    int in = 4;
    int array1[] = new int[4];
    int array2[] = new int[4];
    int array3[] = new int[4];
    int array4[] =new int[4];
    int array5[][] =new int[4][4];



    for (int i = 0; i <4; i++) {

        array1[i] = i+1;
        array2[i] = 4*2 - i;
        array3[i] = 4*2 - i+1;
        array4[i] = 4*4 - i;
    }
    System.out.print(array1);
    System.out.print(array2);
    System.out.print(array3);
    System.out.print(array4);

}
}
