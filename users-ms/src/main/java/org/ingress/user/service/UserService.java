package org.ingress.user.service;

import org.ingress.user.dto.UserDto;
import org.ingress.user.dto.UserResponse;

import java.util.UUID;

public interface UserService {

    UserResponse createUser(UserDto userDto);

    UserResponse updateUser(UserDto userDto, UUID id);
}
