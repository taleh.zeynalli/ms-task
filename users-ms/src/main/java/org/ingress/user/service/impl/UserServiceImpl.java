package org.ingress.user.service.impl;

import lombok.RequiredArgsConstructor;
import org.ingress.user.dto.UserDto;
import org.ingress.user.dto.UserResponse;
import org.ingress.user.coreapi.entity.UserEntity;
import org.ingress.user.coreapi.repository.UserRepository;
import org.ingress.user.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final KafkaMsgPublisherImpl kafkaMsgPublisher;

    @Override
    public UserResponse createUser(UserDto userDto) {
        UserEntity user = new UserEntity();
        BeanUtils.copyProperties(userDto,user);
        userRepository.save(user);
        kafkaMsgPublisher.publish(null,userDto,"user-events");
        UserResponse userResponse = new UserResponse();
        BeanUtils.copyProperties(user,userResponse);
        return userResponse;
    }

    @Override
    public UserResponse updateUser(UserDto userDto, UUID id) {
        Optional<UserEntity> user = userRepository.findById(id);
        BeanUtils.copyProperties(userDto,user.get());
        userRepository.save(user.get());
        UserResponse userResponse = new UserResponse();
        BeanUtils.copyProperties(user.get(),userResponse);
        return userResponse;
    }
}
