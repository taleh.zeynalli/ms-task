package org.ingress.user.projector;


import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;

import org.ingress.user.coreapi.entity.UserEntity;
import org.ingress.user.coreapi.events.UserCreatedEvent;
import org.ingress.user.coreapi.repository.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class UserProjector {

    private final UserRepository repository;

    @EventHandler
    public void on(UserCreatedEvent event) {
        UserEntity user = UserEntity.builder().
                id(event.getId()).
                displayName(event.getDisplayName()).
                email(event.getEmail()).
                password(event.getPassword()).build();
        repository.save(user);
    }

//    @QueryHandler
//    @Transactional
//    public UserView handle(FindUserQuery query) {
//        User user = repository.findById(query.getId()).orElse(null);
//        return mapper.map(user, UserView.class);
//    }


}
