package org.ingress.user.coreapi.repository;

import org.ingress.user.coreapi.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface UserRepository extends CrudRepository<UserEntity, UUID> {


}
