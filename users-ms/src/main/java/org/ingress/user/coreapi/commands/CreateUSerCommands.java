package org.ingress.user.coreapi.commands;

import lombok.Data;

@Data
public class CreateUSerCommands {
    private String displayName;
    private String email;
    private String password;

}
