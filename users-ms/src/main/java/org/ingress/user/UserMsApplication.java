package org.ingress.user;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class UserMsApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(UserMsApplication.class, args);
    }

    @Override
    public void run(String... args)  {
        System.out.println("hello ms");
    }
}
