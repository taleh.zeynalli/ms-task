package org.ingress.user.aggregates;

import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.ingress.user.coreapi.commands.CreateUSerCommands;
import org.ingress.user.coreapi.events.UserCreatedEvent;

import java.util.UUID;

@Aggregate
@NoArgsConstructor
public class UserAggregate {

    @AggregateIdentifier
    private UUID userId;

    private String displayName;
    private String email;
    private String password;


    @CommandHandler
    public UserAggregate(CreateUSerCommands command) {
        UUID aggregateId = UUID.randomUUID();
        AggregateLifecycle.apply(UserCreatedEvent.builder().
                id(aggregateId).
                displayName(command.getDisplayName()).
                email(command.getEmail()).
                password(command.getPassword()).
                build());
    }

    @EventSourcingHandler
    public void on(UserCreatedEvent event) {
        this.userId = event.getId();
        this.displayName = event.getDisplayName();
        this.email=event.getEmail();
        this.password=event.getPassword();
    }

}
