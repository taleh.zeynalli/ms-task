package org.ingress.user.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "user_entity")
@Data
public class UserEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String displayName;
    private String email;
    private String password;

}
