package org.ingress.cards;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class CardMsApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(CardMsApplication.class, args);
    }

    @Override
    public void run(String... args)  {
        System.out.println("hello ms");
    }
}
