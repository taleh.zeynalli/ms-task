package org.ingress.cards.service;

import org.ingress.cards.dto.CardDto;
import org.ingress.cards.dto.CardResponse;


public interface CardService {

    CardResponse createCard(CardDto cardDto);

    CardResponse updateCard(CardDto cardDto,Long id);
}
