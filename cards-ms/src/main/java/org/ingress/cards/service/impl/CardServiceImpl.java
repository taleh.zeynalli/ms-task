package org.ingress.cards.service.impl;

import lombok.RequiredArgsConstructor;
import org.ingress.cards.dto.CardDto;
import org.ingress.cards.dto.CardResponse;
import org.ingress.cards.entity.Card;
import org.ingress.cards.repository.CardRepository;
import org.ingress.cards.service.CardService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;

    @Override
    public CardResponse createCard(CardDto cardDto) {
        Card card = new Card();
        BeanUtils.copyProperties(cardDto,card);
        cardRepository.save(card);
        CardResponse cardResponse = new CardResponse();
        BeanUtils.copyProperties(card,cardResponse);
        return cardResponse;
    }

    @Override
    public CardResponse updateCard(CardDto userDto,Long id) {
        Optional<Card> card = cardRepository.findById(id);
        BeanUtils.copyProperties(userDto,card.get());
        cardRepository.save(card.get());
        CardResponse cardResponse = new CardResponse();
        BeanUtils.copyProperties(card.get(),cardResponse);
        return cardResponse;
    }
}
