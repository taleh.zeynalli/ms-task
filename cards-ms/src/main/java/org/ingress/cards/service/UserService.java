package org.ingress.user.service;

import org.ingress.user.dto.UserDto;
import org.ingress.user.dto.UserResponse;

public interface UserService {

    UserResponse createUser(UserDto userDto);

    UserResponse updateUser(UserDto userDto,Long id);
}
