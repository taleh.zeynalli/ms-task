package org.ingress.cards.listeners.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.ingress.cards.repository.UserRepository;
import org.ingress.common.events.UserCreatedEvent;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class UserRegistrationListener {

    private final UserRepository userRepository;

    @EventHandler
    public void on(UserCreatedEvent event) {
        User user =  userRepository.save(modelMapper.map(event, User.class));
        log.info("CartUserListener AddLoanUser:{}",user);
    }

}
