package org.ingress.cards.listeners.axon;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ingress.cards.dto.UserDto;
import org.ingress.cards.service.UserService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class UserRegistrationAxonListener {

    private final UserService userService;

    @KafkaListener(topics = "user-events",
            groupId = "user",
    containerFactory = "kafkaJsonListenerContainerFactory")
    public void userRegisterListener(UserDto user)  {
        log.info("Message received : {} here is CARDMS", user);
        userService.createUser(user);
    }
}
