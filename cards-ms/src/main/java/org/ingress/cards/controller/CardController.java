package org.ingress.cards.controller;

import lombok.RequiredArgsConstructor;
import org.ingress.cards.dto.CardDto;
import org.ingress.cards.dto.CardResponse;
import org.ingress.cards.service.CardService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("cards")
public class CardController {

    private final CardService cardService;

    @PostMapping
    public ResponseEntity<CardResponse> createCard(@RequestBody CardDto cardDto){
        CardResponse cardResponse = cardService.createCard(cardDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(cardResponse);
    }

    @PutMapping("/{id}")
    public CardResponse updateCard(@RequestBody CardDto cardDto,@PathVariable("id") Long id){
        CardResponse cardResponse = cardService.updateCard(cardDto,id);
        return cardResponse;
    }
}
