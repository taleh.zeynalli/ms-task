package org.ingress.cards.dto;

import lombok.Data;

@Data
public class CardResponse {
    private Long id;
    private String displayName;
    private String email;
    private String password;
}
