package org.ingress.cards.dto;

import lombok.Data;

@Data
public class CardDto {
    private String displayName;
    private String email;
    private String password;
}
