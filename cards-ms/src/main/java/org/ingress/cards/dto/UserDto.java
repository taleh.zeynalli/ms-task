package org.ingress.user.dto;

import lombok.Data;

@Data
public class UserDto {
    private String displayName;
    private String email;
    private String password;
}
