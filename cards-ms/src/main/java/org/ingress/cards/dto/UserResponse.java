package org.ingress.user.dto;

import lombok.Data;

@Data
public class UserResponse {
    private Long id;
    private String displayName;
    private String email;
    private String password;
}
