package org.ingress.cards.repository;


import org.ingress.cards.entity.Card;
import org.springframework.data.repository.CrudRepository;

public interface CardRepository extends CrudRepository<Card, Long> {


}
